#!/usr/bin/env python
# author: tim.schaefer.de@gmail.com


import cv2 as cv
import numpy as np
import time
import rospkg
import os

# get pkg path
rospack = rospkg.RosPack()
rai_sim_path = rospack.get_path('rai_sim')
py_sim_path = rai_sim_path + "/src/py_sim/"

os.chdir(py_sim_path)

print("working in: ",py_sim_path)

# append lib paths
import sys
sys.path.append('.')

import libry as ry

# import ros topic publisher
from publisher.ros_pub import img_publisher, pc2_publisher, joint_publisher


# print opencv version
print(cv.__version__)


#-- Add REAL WORLD configuration and camera
RealWorld = ry.Config()
RealWorld.addFile("./models/baxter_challenge.g")
S = RealWorld.simulation(ry.SimulatorEngine.physx, True)
S.addSensor("camera")

C = ry.Config()
C.addFile('./models/baxter_camera.g')
V = ry.ConfigurationViewer()
V.setConfiguration(C)
cameraFrame = C.frame("camera")

#camera settings
#the focal length
f = 0.895
f = f * 360.
#the relative pose of the camera
# pcl.setRelativePose('d(-90 0 0 1) t(-.08 .205 .115) d(26 1 0 0) d(-1 0 1 0) d(6 0 0 1) ')
fxfypxpy = [f, f, 320., 180.]


points = []
tau = .01


# init img_publisher

img_pub_depth = img_publisher(topic="depth/image_raw",node="node",encoding="32FC1", info_topic="depth/camera_info", fxfypxpy=fxfypxpy)
img_pub_rgb = img_publisher(topic="rgb/image_raw",node="rgb_node",encoding="rgb8")
pc_pub = pc2_publisher(topic="depth/points",node="pc_node")
joint_pub = joint_publisher(node="joint_state_publisher")

# range => simtime in ms
for t in range(200):
    time.sleep(0.01)

    #grab sensor readings from the simulation
    q = S.get_q()
    if t%10 == 0:
        [rgb, depth] = S.getImageAndDepth()  #we don't need images with 100Hz, rendering is slow
        points = S.depthData2pointCloud(depth, fxfypxpy)
        cameraFrame.setPointCloud(points, rgb)
        V.recopyMeshes(C)
        V.setConfiguration(C)
        
        joint_pub.publish(C.getJointNames(), C.getJointState())

        img_pub_depth.publish(depth/depth.max())
        img_pub_rgb.publish(rgb)

        pc_pub.publish(points,rgb,"camera_link")

        #if len(rgb)>0: cv.imshow('OPENCV - rgb', rgb)
        #if len(depth)>0: cv.imshow('OPENCV - depth', 0.5* depth)

        if cv.waitKey(1) & 0xFF == ord('q'):
            break

    S.step([], tau, ry.ControlMode.none)


# wait for input
input()

# When everything done, release the capture
cv.destroyAllWindows()

