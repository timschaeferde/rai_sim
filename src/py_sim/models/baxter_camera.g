Include: './baxter/baxter_new.g'



world {}




table (world){
    shape:ssBox, Q:<t(0. 0.75 .5)>, size:[1. 1. 1. .02], color:[.3 .3 .3]
    contact, logical:{ }
    friction:.1
}


camera(camera_rgb_optical_frame){
    Q:<t(0. 0. 0.) d(180 0 -1 0) d(180 0 0 1)>, color:[.9 .0 .0],
    shape:marker, size:[.4],
    focalLength:0.895, width:640, height:480, zRange:[.4 70]
}
