body base {
}

body torso {
mass:35.336455
inertiaTensor:[1.849155 -0.000354 -0.154188 1.662671 0.003292 0.802239]
}

shape visual torso_1 (torso) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'torso/base_link-simple.STL'
color:[.2 .2 .2 1]
colorName:darkgray
 visual }

shape collision torso_0 (torso) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'torso/base_link_collision-simple.STL'
 contact:-2 }

body left_torso_itb {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body right_torso_itb {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body head {
mass:0.547767
inertiaTensor:[0.004641 0.000159 0.000242 0.003295 -0.001324 0.003415]
}

shape visual head_1 (head) {  
Q:<t(0 0 .00953) E(0 0 0)>
type:mesh mesh:'head/H0-simple.STL'
color:[.2 .2 .2 1]
colorName:darkgray
 visual }

shape collision head_0 (head) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0 0.0) E(0 0 0)>
type:sphere size:[0 0 0 0.001]
 contact:-2 }

body sonar_ring {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual sonar_ring_1 (sonar_ring) {  
Q:<t(-.0347 0 .00953) E(0 0 0)>
type:cylinder size:[0 0 0.01 0.085]
color:[.2 .2 .2 1]
colorName:darkgray
 visual }

shape collision sonar_ring_0 (sonar_ring) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0 0.0) E(0 0 0)>
type:sphere size:[0 0 0 0.001]
 contact:-2 }

body screen {
mass:0.440171
inertiaTensor:[0.004006 0.000230 0.000002 0.002800 0.000029 0.001509]
}

shape visual screen_1 (screen) {  
Q:<t(0 -.00953 -.0347) E(0 -1.57079632679 0)>
type:mesh mesh:'head/H1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision screen_0 (screen) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0 0.0) E(0 0 0)>
type:sphere size:[0 0 0 0.001]
 contact:-2 }

body display {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual display_1 (display) {  
Q:<t(0 0 0) E(0.2617993877991494 0 0)>
type:box size:[0.218 0.16 0.001 0]
color:[0 0 0 1]
colorName:black
 visual }

body camera_rgb_optical_frame {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body head_camera {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body dummyhead1 {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body collision_head_link_1 {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual collision_head_link_1_1 (collision_head_link_1) {  
Q:<t(0 0 0) E(0 0 0)>
type:sphere size:[0 0 0 0.001]
color:[0.85 0.2 0.2 1.0]
colorName:red
 visual }

shape collision collision_head_link_1_0 (collision_head_link_1) {  
 color:[.8 .2 .2 .5],
Q:<t(-0.07 -0.04 0.0) E(0 0 0.0)>
type:sphere size:[0 0 0 0.22]
 contact:-2 }

body collision_head_link_2 {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual collision_head_link_2_1 (collision_head_link_2) {  
Q:<t(0 0 0) E(0 0 0)>
type:sphere size:[0 0 0 0.001]
color:[0.85 0.2 0.2 1.0]
colorName:red
 visual }

shape collision collision_head_link_2_0 (collision_head_link_2) {  
 color:[.8 .2 .2 .5],
Q:<t(-0.07 0.04 0.00) E(0 0 0)>
type:sphere size:[0 0 0 0.22]
 contact:-2 }

body right_arm_mount {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body right_upper_shoulder {
mass:5.70044
inertiaTensor:[0.04709102262 0.00012787556 0.00614870039 0.03766976455 0.00078086899 0.03595988478]
}

shape visual right_upper_shoulder_1 (right_upper_shoulder) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'upper_shoulder/S0-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision right_upper_shoulder_0 (right_upper_shoulder) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0.1361) E(0 0 0)>
type:cylinder size:[0 0 0.2722 0.06]
 contact:-2 }

body right_lower_shoulder {
mass:3.22698
inertiaTensor:[0.01175209419 -0.00030096398 0.00207675762 0.0278859752 -0.00018821993 0.02078749298]
}

shape visual right_lower_shoulder_1 (right_lower_shoulder) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'lower_shoulder/S1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision right_lower_shoulder_0 (right_lower_shoulder) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.12 0.06]
 contact:-2 }

body right_upper_elbow {
mass:4.31272
inertiaTensor:[0.02661733557 0.00029270634 0.00392189887 0.02844355207 0.0010838933 0.01248008322]
}

shape visual right_upper_elbow_1 (right_upper_elbow) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'upper_elbow/E0-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision right_upper_elbow_0 (right_upper_elbow) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 -0.0535) E(0 0 0)>
type:cylinder size:[0 0 0.107 0.06]
 contact:-2 }

body right_upper_elbow_visual {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape collision right_upper_elbow_visual_0 (right_upper_elbow_visual) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0.1365) E(0 0 0)>
type:cylinder size:[0 0 0.273 0.06]
 contact:-2 }

body right_lower_elbow {
mass:2.07206
inertiaTensor:[0.00711582686 0.00036036173 0.0007459496 0.01318227876 -0.00019663418 0.00926852064]
}

shape visual right_lower_elbow_1 (right_lower_elbow) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'lower_elbow/E1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision right_lower_elbow_0 (right_lower_elbow) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0.0 0.0) E(0 0 0)>
type:cylinder size:[0 0 0.10 0.06]
 contact:-2 }

body right_upper_forearm {
mass:2.24665
inertiaTensor:[0.01667742825 0.00018403705 0.00018657629 0.01675457264 -0.00064732352 0.0037463115]
}

shape visual right_upper_forearm_1 (right_upper_forearm) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'upper_forearm/W0-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision right_upper_forearm_0 (right_upper_forearm) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 -0.044) E(0 0 0)>
type:cylinder size:[0 0 0.088 0.06]
 contact:-2 }

body right_upper_forearm_visual {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape collision right_upper_forearm_visual_0 (right_upper_forearm_visual) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0.136) E(0 0 0)>
type:cylinder size:[0 0 0.272 0.06]
 contact:-2 }

body right_arm_itb {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body right_lower_forearm {
mass:1.60979
inertiaTensor:[0.00387607152 -0.00044384784 -0.00021115038 0.00700537914 0.00015348067 0.0055275524]
}

shape visual right_lower_forearm_1 (right_lower_forearm) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'lower_forearm/W1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision right_lower_forearm_0 (right_lower_forearm) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.10 0.06]
 contact:-2 }

body right_wrist {
mass:0.35093
inertiaTensor:[0.00025289155 0.00000575311 -0.00000159345 0.0002688601 -0.00000519818 0.0003074118]
}

shape visual right_wrist_1 (right_wrist) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'wrist/W2-simple.STL'
color:[.1 .1 .1 1]
colorName:lightgrey
 visual }

shape collision right_wrist_0 (right_wrist) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.165 0.06]
 contact:-2 }

body right_hand {
mass:0.19125
inertiaTensor:[0.00017588 0.00000147073 0.0000243633 0.00021166377 0.00000172689 0.00023745397]
}

shape collision right_hand_0 (right_hand) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 -0.0232) E(0 0 0)>
type:cylinder size:[0 0 0.0464 0.04]
 contact:-2 }

body right_hand_camera {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual right_hand_camera_1 (right_hand_camera) {  
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.01 0.02]
color:[0 0 1 0.8]
colorName:blue
 visual }

body right_hand_camera_axis {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body right_hand_range {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual right_hand_range_1 (right_hand_range) {  
Q:<t(0 0 0) E(0 0 0)>
type:box size:[0.005 .02 .005 0]
color:[0 0 1 0.8]
colorName:blue
 visual }

body right_hand_accelerometer {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual right_hand_accelerometer_1 (right_hand_accelerometer) {  
Q:<t(0 0 0) E(0 0 0)>
type:box size:[0.01 0.01 0.01 0]
color:[0 0 0 1]
colorName:black
 visual }

body left_arm_mount {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body left_upper_shoulder {
mass:5.70044
inertiaTensor:[0.04709102262 0.00012787556 0.00614870039 0.03766976455 0.00078086899 0.03595988478]
}

shape visual left_upper_shoulder_1 (left_upper_shoulder) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'upper_shoulder/S0-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision left_upper_shoulder_0 (left_upper_shoulder) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0.1361) E(0 0 0)>
type:cylinder size:[0 0 0.2722 0.06]
 contact:-2 }

body left_lower_shoulder {
mass:3.22698
inertiaTensor:[0.01175209419 -0.00030096398 0.00207675762 0.0278859752 -0.00018821993 0.02078749298]
}

shape visual left_lower_shoulder_1 (left_lower_shoulder) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'lower_shoulder/S1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision left_lower_shoulder_0 (left_lower_shoulder) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.12 0.06]
 contact:-2 }

body left_upper_elbow {
mass:4.31272
inertiaTensor:[0.02661733557 0.00029270634 0.00392189887 0.02844355207 0.0010838933 0.01248008322]
}

shape visual left_upper_elbow_1 (left_upper_elbow) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'upper_elbow/E0-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision left_upper_elbow_0 (left_upper_elbow) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 -0.0535) E(0 0 0)>
type:cylinder size:[0 0 0.107 0.06]
 contact:-2 }

body left_upper_elbow_visual {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape collision left_upper_elbow_visual_0 (left_upper_elbow_visual) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0.1365) E(0 0 0)>
type:cylinder size:[0 0 0.273 0.06]
 contact:-2 }

body left_lower_elbow {
mass:2.07206
inertiaTensor:[0.00711582686 0.00036036173 0.0007459496 0.01318227876 -0.00019663418 0.00926852064]
}

shape visual left_lower_elbow_1 (left_lower_elbow) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'lower_elbow/E1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision left_lower_elbow_0 (left_lower_elbow) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0.0 0.0) E(0 0 0)>
type:cylinder size:[0 0 0.10 0.06]
 contact:-2 }

body left_upper_forearm {
mass:2.24665
inertiaTensor:[0.01667742825 0.00018403705 0.00018657629 0.01675457264 -0.00064732352 0.0037463115]
}

shape visual left_upper_forearm_1 (left_upper_forearm) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'upper_forearm/W0-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision left_upper_forearm_0 (left_upper_forearm) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 -0.044) E(0 0 0)>
type:cylinder size:[0 0 0.088 0.06]
 contact:-2 }

body left_upper_forearm_visual {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape collision left_upper_forearm_visual_0 (left_upper_forearm_visual) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0.136) E(0 0 0)>
type:cylinder size:[0 0 0.272 0.06]
 contact:-2 }

body left_arm_itb {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body left_lower_forearm {
mass:1.60979
inertiaTensor:[0.00387607152 -0.00044384784 -0.00021115038 0.00700537914 0.00015348067 0.0055275524]
}

shape visual left_lower_forearm_1 (left_lower_forearm) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'lower_forearm/W1-simple.STL'
color:[0.85 0.2 0.2 1.0]
colorName:darkred
 visual }

shape collision left_lower_forearm_0 (left_lower_forearm) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.10 0.06]
 contact:-2 }

body left_wrist {
mass:0.35093
inertiaTensor:[0.00025289155 0.00000575311 -0.00000159345 0.0002688601 -0.00000519818 0.0003074118]
}

shape visual left_wrist_1 (left_wrist) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'wrist/W2-simple.STL'
color:[.1 .1 .1 1]
colorName:lightgrey
 visual }

shape collision left_wrist_0 (left_wrist) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.165 0.06]
 contact:-2 }

body left_hand {
mass:0.19125
inertiaTensor:[0.00017588 0.00000147073 0.0000243633 0.00021166377 0.00000172689 0.00023745397]
}

shape collision left_hand_0 (left_hand) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 -0.0232) E(0 0 0)>
type:cylinder size:[0 0 0.0464 0.04]
 contact:-2 }

body left_hand_camera {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual left_hand_camera_1 (left_hand_camera) {  
Q:<t(0 0 0) E(0 0 0)>
type:cylinder size:[0 0 0.01 0.02]
color:[0 0 1 0.8]
colorName:blue
 visual }

body left_hand_camera_axis {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

body left_hand_range {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual left_hand_range_1 (left_hand_range) {  
Q:<t(0 0 0) E(0 0 0)>
type:box size:[0.005 .02 .005 0]
color:[0 0 1 0.8]
colorName:blue
 visual }

body left_hand_accelerometer {
mass:0.0001
inertiaTensor:[1e-08 0 0 1e-08 0 1e-08]
}

shape visual left_hand_accelerometer_1 (left_hand_accelerometer) {  
Q:<t(0 0 0) E(0 0 0)>
type:box size:[0.01 0.01 0.01 0]
color:[0 0 0 1]
colorName:black
 visual }

body pedestal {
mass:60.86397744
inertiaTensor:[5.0635929 0.00103417 0.80199628 6.08689388 0.00105311 4.96191932]
}

shape visual pedestal_1 (pedestal) {  
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'package://baxter_description/meshes/base/PEDESTAL.DAE'
color:[.2 .2 .2 1]
colorName:darkgray
 visual }

shape collision pedestal_0 (pedestal) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0 0) E(0 0 0)>
type:mesh mesh:'package://baxter_description/meshes/base/pedestal_link_collision.DAE'
 contact:-2 }

body left_gripper_base {
mass:0.3
inertiaTensor:[2e-08 0 0 3e-08 0 2e-08]
}

shape visual left_gripper_base_1 (left_gripper_base) {  
Q:<t(0 0 0) E(0 3.14159265359 3.14159265359)>
type:mesh mesh:'package://rethink_ee_description/meshes/pneumatic_gripper/pneumatic_gripper_w_cup.DAE'
 visual }

shape collision left_gripper_base_0 (left_gripper_base) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0.0 0.04) E(0 3.14159265359 3.14159265359)>
type:cylinder size:[0 0 0.08 0.02]
color:[.5 .1 .1 1]
colorName:darkred
 contact:-2 }

body left_gripper {
mass:0.0001
inertiaTensor:[0 0 0 0 0 0.0]
}

body right_gripper_base {
mass:0.3
inertiaTensor:[2e-08 0 0 3e-08 0 2e-08]
}

shape visual right_gripper_base_1 (right_gripper_base) {  
Q:<t(0 0 0) E(-1.57079632679 3.14159265359 0)>
type:mesh mesh:'package://rethink_ee_description/meshes/electric_gripper/electric_gripper_base.DAE'
 visual }

shape collision right_gripper_base_0 (right_gripper_base) {  
 color:[.8 .2 .2 .5],
Q:<t(0.0 0.0 0) E(-1.57079632679 3.14159265359 0)>
type:cylinder size:[0 0 0.1 0.029]
color:[.5 .1 .1 1]
colorName:darkred
 contact:-2 }

body r_gripper_l_finger {
mass:0.02
inertiaTensor:[0.01 0 0 0.01 0 0.01]
}

shape visual r_gripper_l_finger_1 (r_gripper_l_finger) {  
Q:<t(0 0 0) E(0 0 -3.14159265359)>
type:mesh mesh:'package://rethink_ee_description/meshes/electric_gripper/fingers/extended_narrow.DAE'
 visual }

shape collision r_gripper_l_finger_0 (r_gripper_l_finger) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0.01725 0.0615) E(0 0 -3.14159265359)>
type:box size:[0.01 0.0135 0.1127 0]
 contact:-2 }

shape collision r_gripper_l_finger_0 (r_gripper_l_finger) {  
 color:[.8 .2 .2 .5],
Q:<t(-0.005 -0.003 0.0083) E(0 0 -3.14159265359)>
type:box size:[0.01 0.05 0.017 0]
 contact:-2 }

body r_gripper_l_finger_tip {
mass:0.01
inertiaTensor:[0.01 0 0 0.01 0 0.01]
}

shape visual r_gripper_l_finger_tip_1 (r_gripper_l_finger_tip) {  
Q:<t(0 0 0) E(0 0 3.14159265359)>
type:mesh mesh:'package://rethink_ee_description/meshes/electric_gripper/fingers/half_round_tip.DAE'
 visual }

shape collision r_gripper_l_finger_tip_0 (r_gripper_l_finger_tip) {  
 color:[.8 .2 .2 .5],
Q:<t(0 -0.0045 -0.015) E(0 0 3.14159265359)>
type:cylinder size:[0 0 0.037 0.008]
 contact:-2 }

body r_gripper_r_finger {
mass:0.02
inertiaTensor:[0.01 0 0 0.01 0 0.01]
}

shape visual r_gripper_r_finger_1 (r_gripper_r_finger) {  
Q:<t(0 0 0) E(0 0 -0.0)>
type:mesh mesh:'package://rethink_ee_description/meshes/electric_gripper/fingers/extended_narrow.DAE'
 visual }

shape collision r_gripper_r_finger_0 (r_gripper_r_finger) {  
 color:[.8 .2 .2 .5],
Q:<t(0 -0.01725 0.0615) E(0 0 -0.0)>
type:box size:[0.01 0.0135 0.1127 0]
 contact:-2 }

shape collision r_gripper_r_finger_0 (r_gripper_r_finger) {  
 color:[.8 .2 .2 .5],
Q:<t(0.005 0.003 0.0083) E(0 0 -0.0)>
type:box size:[0.01 0.05 0.017 0]
 contact:-2 }

body r_gripper_r_finger_tip {
mass:0.01
inertiaTensor:[0.01 0 0 0.01 0 0.01]
}

shape visual r_gripper_r_finger_tip_1 (r_gripper_r_finger_tip) {  
Q:<t(0 0 0) E(0 0 0.0)>
type:mesh mesh:'package://rethink_ee_description/meshes/electric_gripper/fingers/half_round_tip.DAE'
 visual }

shape collision r_gripper_r_finger_tip_0 (r_gripper_r_finger_tip) {  
 color:[.8 .2 .2 .5],
Q:<t(0 0.0045 -0.015) E(0 0 0.0)>
type:cylinder size:[0 0 0.037 0.008]
 contact:-2 }

body right_gripper {
mass:0.0001
inertiaTensor:[0 0 0 0 0 0.0]
}

joint collision_head_1 (base collision_head_link_1) {  
type:rigid
A:<t(0.11 0 0.75) E(0 0 0)>
}

joint collision_head_2 (base collision_head_link_2) {  
type:rigid
A:<t(0.11 0 0.75) E(0 0 0)>
}

joint dummy (head dummyhead1) {  
type:rigid
A:<t(0 0 0) E(0 0 0)>
}

joint torso_t0 (base torso) {  
type:hingeX
A:<t(0 0 0) E(0 0 0)>
limits:[-1.3963 1.3963]
ctrl_limits:[10000 50000 1]
}

joint left_torso_itb_fixed (torso left_torso_itb) {  
type:rigid
A:<t(-0.08897 0.15593 0.389125) E(-1.57079632679 3.1415 0)>
}

joint right_torso_itb_fixed (torso right_torso_itb) {  
type:rigid
A:<t(-0.08897 -0.15593 0.389125) E(1.57079632679 0 0)>
}

joint head_pan (torso head) {  
type:hingeX
axis:[0 0 1]
A:<t(0.06 0 0.686) E(0 0 0)>
limits:[-1.3963 1.3963]
ctrl_limits:[10000 50000 1]
}

joint head_nod (head screen) {  
type:hingeX
A:<t(.1227 0 0) E(1.75057 0 1.57079632679)>
limits:[-1.3963 1.3963]
ctrl_limits:[10000 50000 1]
}

joint head_camera (head head_camera) {  
type:rigid
A:<t(0.12839 0 0.06368) E(1.75057 0 1.57079632679)>
}

joint display_joint (screen display) {  
type:rigid
A:<t(0.0 -0.016 0.0) E(0 0 0)>
}

joint head_kinect (screen camera_rgb_optical_frame) {  
type:rigid
A:<t(0.073 0.075 0.014) E(-0.843 -0.075 3.075)>
}

joint sonar_s0 (torso sonar_ring) {  
type:rigid
axis:[0 0 1]
A:<t(0.0947 0 .817) E(0 0 0)>
}

joint right_torso_arm_mount (torso right_arm_mount) {  
type:rigid
A:<t(0.024645 -0.219645 0.118588) E(0 0 -0.7854)>
}

joint right_s0 (right_arm_mount right_upper_shoulder) {  
type:hingeX
axis:[0 0 1]
A:<t(0.055695 0 0.011038) E(0 0 0)>
limits:[-1.70167993878 1.70167993878]
ctrl_limits:[1.5 50.0 1]
}

joint right_s1 (right_upper_shoulder right_lower_shoulder) {  
type:hingeX
axis:[0 0 1]
A:<t(0.069 0 0.27035) E(-1.57079632679 0 0)>
limits:[-2.147 1.047]
ctrl_limits:[1.5 50.0 1]
}

joint right_e0 (right_lower_shoulder right_upper_elbow) {  
type:hingeX
axis:[0 0 1]
A:<t(0.102 0 0) E(1.57079632679 0 1.57079632679)>
limits:[-3.05417993878 3.05417993878]
ctrl_limits:[1.5 50.0 1]
}

joint right_e0_fixed (right_lower_shoulder right_upper_elbow_visual) {  
type:rigid
axis:[0 0 1]
A:<t(0.107 0 0) E(1.57079632679 0 1.57079632679)>
}

joint right_e1 (right_upper_elbow right_lower_elbow) {  
type:hingeX
axis:[0 0 1]
A:<t(0.069 0 0.26242) E(-1.57079632679 -1.57079632679 0)>
limits:[-0.05 2.618]
ctrl_limits:[1.5 50.0 1]
}

joint right_w0 (right_lower_elbow right_upper_forearm) {  
type:hingeX
axis:[0 0 1]
A:<t(0.10359 0 0) E(1.57079632679 0 1.57079632679)>
limits:[-3.059 3.059]
ctrl_limits:[4.0 15.0 1]
}

joint right_w0_fixed (right_lower_elbow right_upper_forearm_visual) {  
type:rigid
axis:[0 0 1]
A:<t(0.088 0 0) E(1.57079632679 0 1.57079632679)>
}

joint right_w0_to_itb_fixed (right_upper_forearm right_arm_itb) {  
type:rigid
axis:[0 0 1]
A:<t(-0.0565 0 0.12) E(-1.57079632679 0 1.57079632679)>
}

joint right_w1 (right_upper_forearm right_lower_forearm) {  
type:hingeX
axis:[0 0 1]
A:<t(0.01 0 0.2707) E(-1.57079632679 -1.57079632679 0)>
limits:[-1.57079632679 2.094]
ctrl_limits:[4.0 15.0 1]
}

joint right_w2 (right_lower_forearm right_wrist) {  
type:hingeX
axis:[0 0 1]
A:<t(0.115975 0 0) E(1.57079632679 0 1.57079632679)>
limits:[-3.059 3.059]
ctrl_limits:[4.0 15.0 1]
}

joint right_hand (right_wrist right_hand) {  
type:rigid
axis:[0 0 1]
A:<t(0 0 0.11355) E(0 0 0)>
}

joint right_hand_camera (right_hand right_hand_camera) {  
type:rigid
A:<t(0.03825 0.012 0.015355) E(0 0 -1.57079633)>
}

joint right_hand_camera_axis (right_hand right_hand_camera_axis) {  
type:rigid
A:<t(0.03825 0.012 0.015355) E(0 0 0)>
}

joint right_hand_range (right_hand right_hand_range) {  
type:rigid
A:<t(0.032 -0.020245 0.0288) E(0 -1.57079632679 -1.57079632679)>
}

joint right_hand_accelerometer (right_hand right_hand_accelerometer) {  
type:rigid
A:<t(0.00198 0.000133 -0.0146) E(0 0 0)>
}

joint left_torso_arm_mount (torso left_arm_mount) {  
type:rigid
A:<t(0.024645 0.219645 0.118588) E(0 0 0.7854)>
}

joint left_s0 (left_arm_mount left_upper_shoulder) {  
type:hingeX
axis:[0 0 1]
A:<t(0.055695 0 0.011038) E(0 0 0)>
limits:[-1.70167993878 1.70167993878]
ctrl_limits:[1.5 50.0 1]
}

joint left_s1 (left_upper_shoulder left_lower_shoulder) {  
type:hingeX
axis:[0 0 1]
A:<t(0.069 0 0.27035) E(-1.57079632679 0 0)>
limits:[-2.147 1.047]
ctrl_limits:[1.5 50.0 1]
}

joint left_e0 (left_lower_shoulder left_upper_elbow) {  
type:hingeX
axis:[0 0 1]
A:<t(0.102 0 0) E(1.57079632679 0 1.57079632679)>
limits:[-3.05417993878 3.05417993878]
ctrl_limits:[1.5 50.0 1]
}

joint left_e0_fixed (left_lower_shoulder left_upper_elbow_visual) {  
type:rigid
axis:[0 0 1]
A:<t(0.107 0 0) E(1.57079632679 0 1.57079632679)>
}

joint left_e1 (left_upper_elbow left_lower_elbow) {  
type:hingeX
axis:[0 0 1]
A:<t(0.069 0 0.26242) E(-1.57079632679 -1.57079632679 0)>
limits:[-0.05 2.618]
ctrl_limits:[1.5 50.0 1]
}

joint left_w0 (left_lower_elbow left_upper_forearm) {  
type:hingeX
axis:[0 0 1]
A:<t(0.10359 0 0) E(1.57079632679 0 1.57079632679)>
limits:[-3.059 3.059]
ctrl_limits:[4.0 15.0 1]
}

joint left_w0_fixed (left_lower_elbow left_upper_forearm_visual) {  
type:rigid
axis:[0 0 1]
A:<t(0.088 0 0) E(1.57079632679 0 1.57079632679)>
}

joint left_w0_to_itb_fixed (left_upper_forearm left_arm_itb) {  
type:rigid
axis:[0 0 1]
A:<t(-0.0565 0 0.12) E(-1.57079632679 0 1.57079632679)>
}

joint left_w1 (left_upper_forearm left_lower_forearm) {  
type:hingeX
axis:[0 0 1]
A:<t(0.01 0 0.2707) E(-1.57079632679 -1.57079632679 0)>
limits:[-1.57079632679 2.094]
ctrl_limits:[4.0 15.0 1]
}

joint left_w2 (left_lower_forearm left_wrist) {  
type:hingeX
axis:[0 0 1]
A:<t(0.115975 0 0) E(1.57079632679 0 1.57079632679)>
limits:[-3.059 3.059]
ctrl_limits:[4.0 15.0 1]
}

joint left_hand (left_wrist left_hand) {  
type:rigid
axis:[0 0 1]
A:<t(0 0 0.11355) E(0 0 0)>
}

joint left_hand_camera (left_hand left_hand_camera) {  
type:rigid
A:<t(0.03825 0.012 0.015355) E(0 0 -1.57079633)>
}

joint left_hand_camera_axis (left_hand left_hand_camera_axis) {  
type:rigid
A:<t(0.03825 0.012 0.015355) E(0 0 0)>
}

joint left_hand_range (left_hand left_hand_range) {  
type:rigid
A:<t(0.032 -0.020245 0.0288) E(0 -1.57079632679 -1.57079632679)>
}

joint left_hand_accelerometer (left_hand left_hand_accelerometer) {  
type:rigid
A:<t(0.00198 0.000133 -0.0146) E(0 0 0)>
}

joint pedestal_fixed (torso pedestal) {  
type:rigid
A:<t(0.0 0.0 0.0) E(0 0 0)>
}

joint left_gripper_base (left_hand left_gripper_base) {  
type:rigid
A:<t(0 0 0.0) E(0 0 0)>
}

joint left_endpoint (left_gripper_base left_gripper) {  
type:rigid
A:<t(0 0 0.08) E(0 0 0)>
}

joint r_gripper_l_finger_tip_joint (r_gripper_l_finger r_gripper_l_finger_tip) {  
type:rigid
A:<t(0.0 0.01725 0.1127) E(0 0 0)>
}

joint r_gripper_r_finger_tip_joint (r_gripper_r_finger r_gripper_r_finger_tip) {  
type:rigid
A:<t(0.0 -0.01725 0.1127) E(0 0 0)>
}

joint right_gripper_base (right_hand right_gripper_base) {  
type:rigid
A:<t(0 0 0.025) E(0 0 0)>
}

joint right_endpoint (right_gripper_base right_gripper) {  
type:rigid
A:<t(0 0 0.1327) E(0 0 0)>
}

joint r_gripper_l_finger_joint (right_gripper_base r_gripper_l_finger) {  
type:rigid
axis:[0 1 0]
A:<t(0.0 0.03 0.02) E(0 0 0)>
limits:[0.0 0.020833]
ctrl_limits:[5.0 20.0 1]
}

joint r_gripper_r_finger_joint (right_gripper_base r_gripper_r_finger) {  
type:rigid
mimic:(r_gripper_l_finger_joint)
axis:[0 1 0]
A:<t(0.0 -0.03 0.02) E(0 0 0)>
limits:[-0.020833 0.0]
ctrl_limits:[5.0 20.0 1]
}

