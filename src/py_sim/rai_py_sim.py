#!/usr/bin/env python
# author: tim.schaefer.de@gmail.com

# rai_sim_py class to manage simulation

import cv2 as cv
import numpy as np
import time
import rospkg
import os

# get pkg path
rospack = rospkg.RosPack()
rai_sim_path = rospack.get_path('rai_sim')
py_sim_path = rai_sim_path + "/src/py_sim/"

# change directory 
os.chdir(py_sim_path)
print("working in: ",py_sim_path)


# append lib paths
#import sys
#sys.path.append('.')

# import local library
import libry as ry

# import ros topic publisher
from publisher.ros_pub import img_publisher, pc2_publisher, joint_publisher


###### finished import here ###########

# print opencv version
print(cv.__version__)


class rai_py_sim():
    def __init__(self,real_model="baxter_dbot_objects.g", config_model="baxter_camera.g"):
        
        self.simtime = 0

        #-- Add REAL WORLD configuration and camera
        RealWorld = ry.Config()
        RealWorld.addFile("./models/{}".format(real_model))
        self.S = RealWorld.simulation(ry.SimulatorEngine.physx, True)

        self.S.addSensor("camera")

        self.C = ry.Config()
        self.C.addFile("./models/{}".format(config_model))
        self.V = ry.ConfigurationViewer()
        self.V.setConfiguration(self.C)

        # set camera frame
        self.cameraFrame = self.C.frame("camera")

        #### camera settings #####
        # set height and width
        height=480
        width=640

        #maybe set camera settings here? with config .addSensor

        #the focal length
        f = 0.895
        f = f * height
        #the relative pose of the camera
        # pcl.setRelativePose('d(-90 0 0 1) t(-.08 .205 .115) d(26 1 0 0) d(-1 0 1 0) d(6 0 0 1) ')
        self.fxfypxpy = [f, f, width/2., height/2.]


        self.points = []


        cameraURDFFrame="camera_link"
        self.camerInfoFrame="camera_rgb_optical_frame"

        # init ros topic publishers
        self.img_pub_depth = img_publisher(image_topic="/camera/depth/image",node="node",encoding="mono8", info_topic="/camera/depth/camera_info", fxfypxpy=self.fxfypxpy, frame_id=self.camerInfoFrame)
        self.img_pub_rgb = img_publisher(image_topic="/camera/rgb/image_raw",node="rgb_node",encoding="rgb8")
        self.pc_pub = pc2_publisher(topic="/camera/depth/points",node="pc_node")
        self.joint_pub = joint_publisher(node="joint_state_publisher")

    def simulate(self, simtime, tau=.01):
        # range => simtime in ms
        for t in range(simtime):
            self.sim_step()

        # wait for input
        #input()

        # When everything done, release the capture
        cv.destroyAllWindows()

    def sim_step(self, tau):
        print(self.simtime)
        self.simtime += tau
        # grab sensor readings from the simulation
        q = self.S.get_q()

        # dosen't takes to long to publish (mabe move in if...)
        self.joint_pub.publish(self.C.getJointNames(), self.C.getJointState())

        if self.simtime%10 == 0: # we don't need images with 100Hz, rendering is slow
            self.get_rgb_depth_pointcloud()
            self.update_config_viewer()
            self.publish_ros_topics()
            
            # show images here
            #if len(rgb)>0: cv.imshow('OPENCV - rgb', rgb)
            if len(self.depth)>0: cv.imshow('OPENCV - depth', 0.5* self.depth)
            
            ## press "Q" while cv image is in foreground to exit sim.
            #if cv.waitKey(1) & 0xFF == ord('q'):
            #    break

        self.S.step([], tau, ry.ControlMode.none)
        

    def publish_ros_topics(self):
        # publish rgb and depth image
        self.img_pub_depth.publish((self.depth/self.depth.max() * 255).astype("uint8")) # convert to normalized uint8 picture
        self.img_pub_rgb.publish(self.rgb)

        # publish pointcloud
        self.pc_pub.publish(self.points,self.rgb,self.cameraURDFFrame)

    def update_config_viewer(self):
        self.cameraFrame.setPointCloud(self.points, self.rgb)
        self.V.recopyMeshes(self.C)
        self.V.setConfiguration(self.C)

    def get_rgb_depth_pointcloud(self):
        [self.rgb, self.depth] = self.S.getImageAndDepth()  
        self.points = self.S.depthData2pointCloud(self.depth, self.fxfypxpy)
        

    

