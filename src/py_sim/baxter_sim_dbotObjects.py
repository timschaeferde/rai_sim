#!/usr/bin/env python
# author: tim.schaefer.de@gmail.com


import cv2 as cv
import numpy as np
import time
import rospkg
import os

# get pkg path
rospack = rospkg.RosPack()
rai_sim_path = rospack.get_path('rai_sim')
py_sim_path = rai_sim_path + "/src/py_sim/"

# change directory 
os.chdir(py_sim_path)
print("working in: ",py_sim_path)


# append lib paths
import sys
sys.path.append('.')

import libry as ry

# import ros topic publisher
from publisher.ros_pub import img_publisher, pc2_publisher, joint_publisher


###### finished import here ###########






# print opencv version
print(cv.__version__)


#-- Add REAL WORLD configuration and camera
RealWorld = ry.Config()
RealWorld.addFile("./models/baxter_dbot_objects.g")
S = RealWorld.simulation(ry.SimulatorEngine.physx, True)
S.addSensor("camera")

C = ry.Config()
C.addFile('./models/baxter_camera.g')
V = ry.ConfigurationViewer()
V.setConfiguration(C)

# set camera frame
cameraFrame = C.frame("camera")

#camera settings

# set height and width

height=480
width=640

#maybe set camera settings here?


#the focal length
f = 0.895
f = f * height
#the relative pose of the camera
# pcl.setRelativePose('d(-90 0 0 1) t(-.08 .205 .115) d(26 1 0 0) d(-1 0 1 0) d(6 0 0 1) ')
fxfypxpy = [f, f, width/2., height/2.]


points = []
tau = .01


cameraURDFFrame="camera_link"


# init img_publisher
img_pub_depth = img_publisher(image_topic="/camera/depth/image",node="node",encoding="mono8", info_topic="/camera/depth/camera_info", fxfypxpy=fxfypxpy, frame_id='camera_rgb_optical_frame')
img_pub_rgb = img_publisher(image_topic="/camera/rgb/image_raw",node="rgb_node",encoding="rgb8")
pc_pub = pc2_publisher(topic="/camera/depth/points",node="pc_node")
joint_pub = joint_publisher(node="joint_state_publisher")




# range => simtime in ms
for t in range(20000):

    # grab sensor readings from the simulation
    q = S.get_q()
    if t%10 == 0:
        [rgb, depth] = S.getImageAndDepth()  #we don't need images with 100Hz, rendering is slow
        points = S.depthData2pointCloud(depth, fxfypxpy)
        cameraFrame.setPointCloud(points, rgb)
        V.recopyMeshes(C)
        V.setConfiguration(C)
        
        joint_pub.publish(C.getJointNames(), C.getJointState())

        img_pub_depth.publish((depth/depth.max() * 255).astype("uint8"))
        img_pub_rgb.publish(rgb)

        pc_pub.publish(points,rgb,cameraURDFFrame)
        
        # show images here
        #if len(rgb)>0: cv.imshow('OPENCV - rgb', rgb)
        if len(depth)>0: cv.imshow('OPENCV - depth', 0.5* depth)
        
        if cv.waitKey(1) & 0xFF == ord('q'):
            break

    S.step([], tau, ry.ControlMode.none)


# wait for input
#input()

# When everything done, release the capture
cv.destroyAllWindows()

