
#!/usr/bin/env python
# author: tim.schaefer.de@gmail.com

import rospkg
import os
import sys

print("Python version")
print (sys.version)


# is this needed???

# get pkg path
rospack = rospkg.RosPack()
rai_sim_path = rospack.get_path('rai_sim')
py_sim_path = rai_sim_path + "/src/py_sim/"

# change directory 
os.chdir(py_sim_path)

print("working in: ",py_sim_path)


# append lib paths
#import sys
sys.path.append('.')

from rai_py_sim import rai_py_sim


###### finished import here ###########


sim = rai_py_sim(real_model="baxter_dbot_objects.g", config_model="baxter_camera.g")


tau=.01
simtime=200/tau

# range => simtime in ms
for t in range(int(simtime)):
    #sim.C.setJoint....
    sim.sim_step(tau)
    


