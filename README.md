# This Project was merged to: https://animal.informatik.uni-stuttgart.de/yoojin.oh/rai_baxter/

# rai-sim ros-package for simulating baxter robot

This is a ros-package with python-code, launh-files and rviz-configs for Simulation of the Baxterrobot with the [rai-framework](https://github.com/MarcToussaint/rai). The visualisation contains the robot, a table and objects. A camera provides RGB images, depth images and a pointcloud with RGB information.


# Getting Started
1. Create a virtual enviroment to make sure you use python3 and activate it.

2. Then you need to checkout and compile the rai libary from [robotics-course](https://github.com/MarcToussaint/robotics-course). You'll find all information in the README. Maybe just compiling from the [rai](https://github.com/MarcToussaint/rai) repo is enoght (not tested).

3. Next clone the package into your catkin workspace.
	```bash 
	cd $BAXTER_CATKIN_WS
	git clone https://gitlab.com/timschaeferde/rai_sim_pkg.git ./rai_sim
	cd rai_sim
	```
  

4. We need to link the compiled libry library to the ``py_sim`` directory.
	```bash
	cd src/py_sim/
	ln -s ~/git/robotics-course/rai/?????????.so libry.so
	```

5. Then build your catkin workspace with ``catkin_make``
6. start the sim:
	```bash
	roslaunch rai_sim rai_sim_rviz.launch
	```

7. start dbot tracking with:
	```bash
	roslaunch dbot_ros particle_tracker.launch

	```

8. start dbrt with:
	```bash
	roslaunch dbrt_baxter launch_baxter_gpu.launch

	```


# Configuration 

You can change all ros-related things in the launchfiles in ```launch/*``` and change which pyhton script is loaded in the file ```scripts/start_sim.py```.


# The simulation code

All the python code for the simulation is in ```src/py_sim```

* in the subfolder ```models``` you can find all the robot models for the rai sim
* in the subfolder ```publisher``` you can find the ros-topic-publishers for the data from the sim
* the files ```baxter_sim_*``` are the simulation scripts where the models are loaded and the simulation is handelt



Maintained by tim.schaefer.de@gmail.com

